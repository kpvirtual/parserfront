import { createHashHistory  } from 'history';

/* returns object to navigate pages/routes */
const history = createHashHistory ();

/* returns base url */
const API_URL = '';


export {
    API_URL,
}

export default history;
