import React, { useState, useEffect } from "react";

import Select from "react-select";

 

import Chip from "@material-ui/core/Chip";

import Autocomplete from "@material-ui/lab/Autocomplete";

import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

import TextField from "@material-ui/core/TextField";

// import Stack from '@mui/material/Stack';

 

import {

  CButton,

  CCard,

  CCardBody,

  CCardHeader,

  CCol,

  CRow,

  CTable,

  CTableBody,

  CTableCaption,

  CTableDataCell,

  CTableHead,

  CTableHeaderCell,

  CTableRow,

  CSpinner,

  CForm,

  CFormCheck,

  CFormInput,

  CFormLabel,

  CFormSelect,

  CInputGroup,

  CInputGroupText,

} from "@coreui/react";

 

import { DocsExample } from "src/components";

import { reset } from "enzyme/build/configuration";

 

const options = [

  { id: 1, value: "PHP", label: "PHP" },

  { id: 2, value: "Laravel", label: "Laravel" },

  { id: 3, value: "Codeigniter", label: "Codeigniter" },

  { id: 4, value: "Zend", label: "Zend" },

  { id: 5, value: "Cake PHP", label: "Cake PHP" },

  { id: 6, value: "Nodejs", label: "Nodejs" },

  { id: 7, value: "MySQL", label: "MySQL" },

  { id: 8, value: "Reactjs", label: "Reactjs" },

  { id: 9, value: "Angular", label: "Angular" },

  { id: 10, value: "JavaScript", label: "JavaScript" },

  { id: 11, value: "JQuery", label: "JQuery" },

];

 

const top100Films = [

  { title: 'Php' },

  { title: 'Ptyhon'},

  { title: 'MySql' },

  { title: 'Sql'},

  { title: 'Mongo DB' },

  { title: "Node Js" },

  { title: "Java" }

];




const ResumeParserList = () => {

  const [resumedata, setResumeData] = useState([]);

  const [error, setError] = useState(null);

  const [isLoaded, setIsLoaded] = useState(false);

  

  

  

 

  const [name, setName] = useState("");

  const [email, setEmail] = useState("");

  const [skills, setSkills] = useState("");

  const [skillsOptions, setSkillsOptions] = useState([]);

  const [phone, setPhone] = useState("");

  const [city, setCity] = useState("");

  const [company, setCompany] = useState("");

  const [experience, setExperience] = useState("");

  const [skillsSearch, setSkillsSearch] = useState('');

  const [skillsData, setSkillsData] = useState([]);

 

  useEffect(() => {
      fetchData()
  }, []);

 const fetchData = () => {
  fetch("http://localhost:8080/api/resumes/search")

  .then((res) => res.json())



  .then(

    (result) => {

      setIsLoaded(true);

      setResumeData(result.userResumes);

      setSkillsOptions(result.skills);

    },



    (error) => {

      setIsLoaded(true);

      setError(error);

    }

  );
 }

  if (error) {

    return <div>Error: {error.message}</div>;

  } else if (!isLoaded) {

    return (

      <div>

        <CSpinner color="dark" />

      </div>

    );

  } else {

    const resetForm = () => {

      // document.getElementById("create-course-form").reset();

      // location.reload();

      document.getElementsByClassName('MuiAutocomplete-clearIndicator')[0].click();

      setName('')

      setEmail('')
      setCompany('')
      setPhone('')
      setCity('')
      // setSkillsData([])

      // setSkillsSearch('')

      fetchData()

      

    };

 

    const handleSubmit = (event) => {

      const requestOptions = {

        method: "GET",

 

        headers: { "Content-Type": "application/json" },

      };

 

      fetch(

        `http://localhost:8080/api/resumes/search?name=${encodeURIComponent(

          name

        )}&email=${encodeURIComponent(email)}&skills=${encodeURIComponent(skillsSearch)}&phone=${encodeURIComponent(phone)}&company=${encodeURIComponent(company)}&city=${encodeURIComponent(city)}`,

        requestOptions

      )

        .then((res) => res.json())

 

        .then(

          (result) => {

            setIsLoaded(true);

            setResumeData(result.userResumes);

          },

          (error) => {

            setIsLoaded(true);

            setError(error);

          }

        );

    };

 

    // handle onChange event of the dropdown

  // const handleChange = e => {

  //   setSkillsData(e);

  //   let skillsData = [];

    

  //   if(e){

  //     skillsData = e.map((data) => {

  //       return data.value

  //     })

  //   }

  //   setSkillsSearch(skillsData);

  // }

 

    return (

      <CRow>

        <CCol xs={12}>

          <CCard className="mb-4">

            <CCardHeader>

              <strong>Resume</strong> <small>Search Form</small>

            </CCardHeader>

 

            <CCardBody>

              <DocsExample href="">

                <CForm

                  className="row gx-3 gy-2 align-items-center"

                  id="create-course-form"

                >

                  {/* <CCol sm={3}>

                    <CFormLabel

                      className="visually-hidden"

                      htmlFor="specificSizeInputName"

                    >

                      Name

                    </CFormLabel>

 

                    <CFormInput

                      id="specificSizeInputName"

                      onChange={(e) => setName(e.target.value)}

                      placeholder="Name"

                    />

                  </CCol> */}

 

                  <CCol sm={3}>

                    <CFormLabel

                      className="visually-hidden"

                      htmlFor="specificSizeInputGroupUsername"

                    >

                      Email

                    </CFormLabel>

 

                    <CInputGroup>

                      {/* <CInputGroupText>@</CInputGroupText> */}

 

                      <CFormInput

                        id="specificSizeInputGroupUsername"

                        onChange={(e) => setEmail(e.target.value)}

                        placeholder="Email"

                        value={email}

                      />

                    </CInputGroup>

                  </CCol>

                  <CCol sm={3}>

                    <CFormLabel

                      className="visually-hidden"

                      htmlFor="specificSizeInputPhone"

                    >

                      Phone

                    </CFormLabel>

 

                    <CFormInput

                      id="specificSizeInputPhone"

                      placeholder="Phone"

                      value={phone}

                      onChange={(e) => setPhone(e.target.value)}

                    />

                  </CCol>

                  <CCol sm={3}>

                    <CFormLabel

                      className="visually-hidden"

                      htmlFor="specificSizeInputCity"

                    >

                      City

                    </CFormLabel>

 

                    <CFormInput

                      id="specificSizeInputCity"

                      placeholder="City"

                      value={city}

                      onChange={(e) => setCity(e.target.value)}

                    />

                  </CCol>

                  {/* <CCol sm={3}>

                    <CFormLabel

                      className="visually-hidden"

                      htmlFor="specificSizeInputExperience"

                    >

                      Work Experience

                    </CFormLabel>

 

                    <CFormInput

                      id="specificSizeInputExperience"

                      placeholder="Work Experience"

                      value={experience}

                    />

                  </CCol> */}

                  <CCol sm={3}>

                    <CFormLabel

                      className="visually-hidden"

                      htmlFor="specificSizeInputCompany"

                    >

                      Company

                    </CFormLabel>

                    

                    <CFormInput

                      id="specificSizeInputCompany"

                      placeholder="Company"

                      value={company}

                      onChange={(e) => setCompany(e.target.value)}

                    />

                  </CCol>

                   {/*<CCol sm={3}>

                    <Select

                      isMulti

                      options={skillsOptions}

                      onChange={handleChange}

                      value={skillsOptions.find((obj) => obj.value === skills)}

                    />*/}

 

                  <CCol sm={3}>

                 

                    <Autocomplete

                      multiple

                      id="tags-filled"

                      options={skillsOptions.map((option) => option.value)}

                      onChange={(event, value) => setSkillsSearch(value) } 

                      freeSolo

                      renderTags={(value, getTagProps) =>

                        value.map((option, index) => (

                          <Chip variant="outlined" label={option} {...getTagProps({ index })} />

                        ))

                      }

                      

                      renderInput={(params) => (

                        <TextField

                          {...params}

                          variant="filled"

                          placeholder="Skills"

                        />

                      )}

                    />

      

                  </CCol>

                  {/* <CCol xs="auto">

 

                  <CFormCheck type="checkbox" id="autoSizingCheck2" label="Remember me" />

 

                </CCol> */}

 

                  <CCol xs="auto">

                    <CButton type="button" onClick={handleSubmit}>

                      Submit

                    </CButton>

                  </CCol>

                  <CCol xs="auto">

                    <CButton type="button" onClick={resetForm}>

                      Reset

                    </CButton>

                  </CCol>

                </CForm>

              </DocsExample>

            </CCardBody>

          </CCard>

        </CCol>

 

        <CCol xs={12}>

          <CCard className="mb-4">

            <CCardHeader>

              <strong>Resume Information</strong>

            </CCardHeader>

 

            <CCardBody>

              <CTable>

                <CTableHead>

                  <CTableRow>

                    <CTableHeaderCell scope="col">#</CTableHeaderCell>

 

                    <CTableHeaderCell scope="col">Name</CTableHeaderCell>

 

                    <CTableHeaderCell scope="col">Email</CTableHeaderCell>

 

                    <CTableHeaderCell scope="col">Phone</CTableHeaderCell>

 

                    <CTableHeaderCell scope="col">Action</CTableHeaderCell>

                  </CTableRow>

                </CTableHead>

 

                <CTableBody>

                  {resumedata.map((data, index) => (

                    <CTableRow key={index}>

                      <CTableDataCell>{index}</CTableDataCell>

 

                      <CTableDataCell>{data.name}</CTableDataCell>

 

                      <CTableDataCell>{data.email}</CTableDataCell>

 

                      <CTableDataCell>{data.phone}</CTableDataCell>

 

                      <CTableDataCell>

                        {/* <Link to={`/base/resumeparserview/?id=${data._id}`} class="btn btn-primary" type="button">View</Link> */}

 

                        {data.resumePath ? (

                          <a

                            href={`http://localhost:8080/api/resumes/view/${data.resumePath}`}

                            className="btn btn-primary"

                            target="_blank"

                          >

                            View

                          </a>

                        ) : (

                          "N/A"

                        )}

                      </CTableDataCell>

                    </CTableRow>

                  ))}

                </CTableBody>

              </CTable>

            </CCardBody>

          </CCard>

        </CCol>

      </CRow>

    );

  }

};

 

export default ResumeParserList;