import React, { useState, useEffect } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CForm,
  CFormCheck,
  CFormInput,
  CFormTextarea,
  CFormLabel,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'
import { DocsCallout, DocsExample } from 'src/components'
import {useLocation} from "react-router-dom";

const ResumeParserView = (props) => {
  const search = useLocation().search;
  const id = new URLSearchParams(search).get('id');

  const [resumedata, setResumeData] = useState([]);
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    
    fetch(`http://localhost:8080/api/resumes/${id}`)
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setResumeData(result);
          console.log(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          //console.log("error "+error);
          setIsLoaded(true);
          setError(error);
        }
      )
  }, [])

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Layout</strong> <small>Gutters</small>
          </CCardHeader>
          <CCardBody>
            <DocsExample href="forms/layout#gutters">
              <CRow className="g-3">
                <CCol md={12}>
                  <CFormLabel htmlFor="inputName4">Name</CFormLabel>
                  <CFormInput type="text" id="inputName4" value={resumedata.name}/>
                </CCol>
                <CCol md={6}>
                  <CFormLabel htmlFor="inputEmail4">Email</CFormLabel>
                  <CFormInput type="email" id="inputEmail4" value={resumedata.email}/>
                </CCol>
                <CCol md={6}>
                  <CFormLabel htmlFor="inputPhone4">Phone</CFormLabel>
                  <CFormInput type="text" id="inputPhone4" value={resumedata.phone}/>
                </CCol>
                <CCol xs={12}>
                  <CFormLabel htmlFor="inputobjective">Objective</CFormLabel>
                  <CFormTextarea id="inputobjective" placeholder="" rows="5" value={resumedata.objective}></CFormTextarea>
                  {/* <CFormInput id="inputobjective" placeholder="" value={resumedata.objective}/> */}
                </CCol>
                <CCol xs={12}>
                  <CFormLabel htmlFor="inputWorkExperience2">Work Experience</CFormLabel>
                  <CFormInput id="inputWorkExperience2" placeholder="" value={resumedata.totalYearsExperience?.numberInt}/>
                </CCol>
                <CCol md={12}>
                  <CFormLabel htmlFor="inputSkills">Skills</CFormLabel>
                  <CCardHeader>
                  {Array.apply(0, resumedata.skills).map((data, index) => (
                      <CButton className="mt-2" color="dark" shape="rounded-pill" key={index}>
                        {data}
                      </CButton>
                    ))}
                  </CCardHeader>
                  
                </CCol>
              </CRow>
            </DocsExample>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default ResumeParserView
