import React,{ useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
} from '@coreui/react'
import Dropzone from 'react-dropzone-uploader'
import 'react-dropzone-uploader/dist/styles.css'

import history from 'src/utils/helper'

const ResumeAdd = () => {
  let fileArray = [];
  const [filesArr, setFilesArr] = useState([]);
  const getUploadParams = ({ file }) => {
    const body = new FormData();
    body.append('dataFiles', file);
    return { url: 'http://localhost:8080/api/resumes/', body }
  }

  const handleChangeStatus = ({ xhr },status) => {
    if (xhr) {
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          const result = JSON.parse(xhr.response);
          fileArray.push(result.files[0].filename);
        }
      }
    }
    if (status === 'removed') {
      const result = JSON.parse(xhr.response);
      const index = fileArray.indexOf(result.files[0].filename);
      if (index > -1) {
        fileArray.splice(index, 1);
      }
    }
  }

  const handleSubmit = (files, allFiles) => {
    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        "filename": fileArray.toString()
      })
    };
    fetch("http://localhost:8080/api/resumes/parse",requestOptions)
      .then(res => res.json())
      .then(
        (result) => {
          history.push('/resumeparser')
        },
        (error) => {
          console.log("error ",error);
        }
      )
      allFiles.forEach(f => f.remove())
  }
  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Resume</strong> <small>Add</small>
          </CCardHeader>
          <CCardBody>
          <Dropzone
            getUploadParams={getUploadParams}
            onChangeStatus={handleChangeStatus}
            onSubmit={handleSubmit}
            styles={{ dropzone: { minHeight: 200, maxHeight: 250 } }}
          />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default ResumeAdd
